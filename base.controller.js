(function () {

    'use strict';

    /**
     * @ngdoc function
     * @name bravoureAngularApp.controller:CommonController
     * @description
     * # CommonController
     * Controller of the bravoureAngularApp
     */


    function baseController($scope, APP_CONFIG, DATA, PATH_CONFIG, IMAGE_SIZE, $stateParams, $rootScope, $translate, $timeout, $state, $location, $sce) {

        var vm = this;

        vm.PATH_CONFIG = PATH_CONFIG;
        vm.APP_CONFIG = APP_CONFIG;
        vm.DATA = DATA;
        vm.IMAGE_SIZE = IMAGE_SIZE;
        vm.UTIL = $rootScope.UTIL;

        $scope.vm = vm;

        vm.PATH_CONFIG.HTTP_HOST = host;

        for (var i = 0; i < APP_CONFIG.length; i++) {
            if (APP_CONFIG[i].id == $stateParams.pageId) {
                vm.page = APP_CONFIG[i];
                vm.modules = vm.page.modules;
            }
        }

        // Creates the links from the identifier and parameters provided
        $scope.getLink = function (identifier, params) {
            return $state.href('top.' + identifier, params);
        };
        // Creates the links from the identifier and parameters provided
        $scope.getLinkAbsolute = function (identifier, params) {
            return $state.href(identifier, params);
        };

        // Creates the links from the identifier and parameters provided
        $scope.getLinkEvents = function (identifier, params) {
            return $state.href(identifier, params);
        };

        var $windowWidth = $(window).width();

        $(window).resize(function (){
            $windowWidth = $(window).width();
        });

        var disableAnimationMobile = data.disableAnimationMobile;

        // Animation library
        $scope.animateElementIn = function ($el) {
            if (!disableAnimationMobile) {
                animations ($el);
            } else {
                if ($windowWidth > 768) {
                    animations ($el);
                } else {
                    $el.addClass('no-transition');
                }
            }
        };
        // Animation library
        $scope.animateElementInNoDelay = function ($el) {
            if ($windowWidth > 768) {
                animationsNoDelay ($el);
            }
        };
        // Animation library
        $scope.animateElementInMobile = function ($el) {
            animationsMobile ($el);
        };

        function  animationsMobile ($el) {

            var delay = $el.attr('data-anim-delay');

            if (delay != '' || delay != undefined) {
                $timeout(function () {
                    $el.removeClass('anim-hide-mobile');
                    $el.addClass('animated anim__fade-in');
                }, delay);
            } else {
                $el.removeClass('anim-hide-mobile');
                $el.addClass('animated anim__fade-in');
            }

        }

        function  animations ($el) {
            $el.removeClass('anim-hide');
            $el.addClass('animated anim__fade-in');
        }

        function  animationsNoDelay ($el) {
            $el.removeClass('anim-hide');
            $el.addClass('animated anim__fade-in');
        }

        $scope.$state = $state;

        // Creates the links from the identifier and parameters provided
        $scope.getRouterLink = function (identifier, scope) {

            for (var i = 0; i < APP_CONFIG.length; i++) {
                if (typeof APP_CONFIG[i].content.routes != 'undefined') {
                    for (var j = 0; j < APP_CONFIG[i].content.routes.length; j++) {
                        if (APP_CONFIG[i].content.routes[j].identifier == identifier) {
                            var routeArray = APP_CONFIG[i].content.routes[j].route.split('/');
                            var dataSource = scope.content != null ? scope.content : scope;
                            var slug = scope.content != null ? dataSource.url_slug.split('/')[routeArray.indexOf('{slug}')] : scope.slug;
                        }
                    }
                }
            }

            var params = {
                id: scope.id,
                slug: slug
            };

            var result = identifier + '({id: ' + scope.id + ', slug: \'' + slug + '\'})';


            return result;
        };

        $scope.getValueAPi = function (scope, value1) {

            if (typeof scope.content != 'undefined') {
                return scope.content[value1];
            } else if (typeof scope[value1] != 'undefined') {
                return scope[value1];
            }
        };

        $scope.getDate = function (date) {
            if (date == 'now') {
                return new Date();
            } else if (date != undefined) {
                var sDateTime = date;
                var sDateTimeSplitted = sDateTime.split(/[^0-9]/);
                var oValidDateTime = new Date(sDateTimeSplitted[0], sDateTimeSplitted[1] - 1, sDateTimeSplitted[2], sDateTimeSplitted[3], sDateTimeSplitted[4], sDateTimeSplitted[5]);
                return oValidDateTime;
            } else {
                return false;
            }
        };

        $scope.getSlugString = function (value) {
            value = value.replace(/[^\w\s-]/g, "").trim().toLowerCase();
            return value.replace(/[-\s]+/g, "-");
        };

        $scope.getTitle = function (scope) {
            if (!angular.isUndefined(scope) && !angular.isUndefined(scope.main_title) && scope.main_title != '') {
                return scope.main_title;
            } else {

                if (!angular.isUndefined(scope) && !angular.isUndefined(scope.title) && scope.title != '') {
                    return scope.title;
                } else {
                    return false;
                }
            }
        };

        // Trust iframe content
        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        };


        $scope.getLang = function (item) {
            var lang = $translate.use();
            return item[lang];
        };

        $scope.getSlug = function (slug) {
            var slugRes = slug.replace('en/', '').replace('/', '');
            return slugRes;
        };

        $scope.getAltText = function (scope) {
            if(scope != 'undefined' && scope != null) {
                if (typeof scope.main_alt_text != 'undefined' && scope.main_alt_text != '') {
                    return scope.main_alt_text;
                } else if (typeof scope.main_title != 'undefined' && scope.main_title != '') {
                    return scope.main_title;
                } else if (typeof scope.title != 'undefined' && scope.title != '') {
                    return scope.title;
                } else {
                    return '';
                }
            }
        };

        $scope.getFirstImage = function (image) {
            if (image.length > 0) {
                return image[0];
            } else {
                return image;
            }
        };

        // Creates the links from the identifier and parameters provided
        $scope.getModuleLink = function (identifier, module) {
            var params = {};

            if (typeof module != 'undefined') {

                var id = (module.id != '') ? module.id : '';
                var slug = (
                    typeof module.content != 'undefined' &&
                    typeof module.content.url != 'undefined' &&
                    typeof module.content.url_slug != 'undefined'
                ) ? module.content.slug : 'slug';

                params = {
                    id: id,
                    slug: slug
                };
            }

            return $state.href(module.content.handle, params);
        };

        // Creates the videoId from the embeded url from the CMS
        $scope.processVideoEmbed = function (string) {
            var youtubeString = 'youtube_';

            // Youtube embed
            if (string.indexOf(youtubeString) != -1) {
                return string.replace(youtubeString, '');
            }

            return false;
        };

        $scope.getSpotifyIframe = function (url) {
            return '<iframe src="https://embed.spotify.com/?uri=' + url + '" scrolling="no" frameborder="0" width="100%" height="300px"></iframe>';
        };

        // Creates the videoId from the embeded url from the CMS
        $scope.processAudioEmbed = function (string, size, returnType) {
            var soundcloud_track = 'soundcloud_track_';
            var soundcloud_playlist = 'soundcloud_playlist_';
            var soundcloud_user = 'soundcloud_user_';
            var url;

            switch (true) {
                case (string.indexOf(soundcloud_track) != -1) :
                    url = 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/' + string.replace(soundcloud_track, '') + '&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true';
                    break;
                case (string.indexOf(soundcloud_playlist) != -1) :
                    url = 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/' + string.replace(soundcloud_playlist, '') + '&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true';
                    break;
                case (string.indexOf(soundcloud_user) != -1) :
                    url = 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/users/' + string.replace(soundcloud_user, '') + '&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true';
                    break;
                default:
                    return false;
                    break;
            }

            var sizeIframe = (size == 'small') ? 166 : 450;

            if (typeof returnType != 'undefined') {
                return url;
            } else {
                return '<iframe src="' + url + '" class="soundcloud_iframe" scrolling="no" frameborder="0" width="100%" height="' + sizeIframe + '"></iframe>';
            }

        };

        $scope.convertVideoUrl = function (component, url, width, returnType) {

            var placeholder = (typeof component == 'object') ? component.find('.video-player__placeholder') : '';
            /*
             var vimeoUrl = /(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/g;
             var youtubeUrl = /(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:embed\?v=)?(.+)/g;
             */
            var replacement;
            var video_url;

            switch (true) {
                // Youtube
                case (url.indexOf('youtube') != -1):
                    video_url = 'https://www.youtube.com/embed/' + url.replace('youtube_', '') + '?portrait=0&rel=0&autoplay=1';
                    break;

                case (url.indexOf('vimeo') != -1):
                    video_url = 'https://player.vimeo.com/video/' + url.replace('vimeo_', '') + '?title=1&byline=1&portrait=1&autoplay=true';
                    break;
            }

            if (typeof returnType != 'undefined') {
                return video_url;
            } else {
                replacement = '<iframe width="' + width + '" src="' + video_url + '" frameborder="0"></iframe>';

                var placeholderReturn = placeholder.append(replacement).find('iframe').fadeIn('slow');

                return placeholderReturn;
            }
        };

        $scope.getImage = function (scope, string) {

            var result;

            for (var key in scope) {

                if(string != 'undefined') {
                    var splitted = key.toLowerCase().split('__');

                    if (typeof splitted[0] != 'undefined' && string != null && string != 'undefined') {
                        if (splitted[0].toLowerCase() == string.toLowerCase()) {
                            result = scope[key];
                        }
                    }
                }

            }

            result = (result != '' && result != undefined) ? result : false;

            return result;
        };

        // TODO[FE] - To be replaced // Methods to provide the string of the method to translate
        // Methods to provide the string of the method to translate
        $scope.getTranslateUrlTitle = function (translateArray, num) {

            if (typeof translateArray == 'string') {

                var type = (typeof num != 'undefined') ? num : 'title';

                if (typeof $scope.langs != 'undefined' &&
                    typeof $scope.langs[$translate.use()] != 'undefined' &&
                    typeof $scope.langs[$translate.use()][translateArray] != 'undefined' &&
                    typeof $scope.langs[$translate.use()][translateArray][0] != 'undefined') {

                    return $scope.langs[$translate.use()][translateArray][0][type];
                } else {

                    if (typeof $scope.langs != 'undefined' &&
                        typeof $scope.langs[data.default_lang][translateArray] != 'undefined' &&
                        typeof $scope.langs[data.default_lang][translateArray][0] != 'undefined') {

                        if ($translate.use() != [data.default_lang]) {
                            console.log($scope.type + ': ' + translateArray + ' is not translate to ' + $translate.use());
                        }

                        return $scope.langs[data.default_lang][translateArray][0][type];
                    } else {
                        if (typeof $scope.type != 'undefined') {
                            //console.log($scope.type + ': ' + translateArray + ' not found to ' + $translate.use());
                        }
                        return false;
                    }

                }

            }
        };

        // Methods to provide the string of the method to translate
        $scope.getTranslateUrlLink = function (translateArray, num) {

            if (typeof translateArray == 'string') {

                var type = (typeof num != 'undefined') ? num : 'uri';

                if (typeof $scope.langs != 'undefined' &&
                    typeof $scope.langs[PATH_CONFIG.current_language] != 'undefined' &&
                    typeof $scope.langs[PATH_CONFIG.current_language][translateArray] != 'undefined' &&
                    typeof $scope.langs[PATH_CONFIG.current_language][translateArray][0] != 'undefined') {

                    var href = $scope.langs[PATH_CONFIG.current_language][translateArray][0][type];
                    var finalUrl = href;

                    if (href.indexOf("entity") != 0) {
                        finalUrl = href;
                    } else {

                        var pageId = href.substr(href.lastIndexOf('/') + 1);
                        var url = host + data.apiVersion + 'page/' + pageId;

                        for (var i = 0; i < APP_CONFIG.pages.length; i++) {
                            if (APP_CONFIG.pages[i].url == url) {
                                var identifier = APP_CONFIG.pages[i].identifier;
                                finalUrl = $state.href(identifier + '_' + PATH_CONFIG.current_language);

                            }
                        }

                    }


                    return finalUrl;

                } else {

                    if (typeof $scope.langs != 'undefined' &&
                        typeof $scope.langs.en[translateArray] != 'undefined' &&
                        typeof $scope.langs.en[translateArray][0] != 'undefined') {

                        if (PATH_CONFIG.current_language != 'en') {
                            console.log($scope.type + ': ' + translateArray + ' is not translate to ' + PATH_CONFIG.current_language);
                        }

                        return $scope.langs.en[translateArray][0][type];
                    } else {
                        if (typeof $scope.type != 'undefined') {
                            //console.log($scope.type + ': ' + translateArray + ' not found to ' + PATH_CONFIG.current_language);
                        }
                        return false;
                    }

                }

            }
        };


        /* Method to get the correct translation in and i18n object */
        $scope.getTranslate = function (translateArray, num) {

            if (typeof translateArray == 'object') {
                return translateArray[PATH_CONFIG.current_language];
            }
        };


        /* Old methods */
        $scope.getTranslateLink = function (identifier, params) {
            return $state.href(identifier + '_' + PATH_CONFIG.current_language, params);
        };


        // TODO [] - Check where it is used
        $scope.config = $rootScope.config;

        // TODO [] - Check where it is used
        $scope.date = new Date();

        // TODO [] - Check where it is used
        $scope.currentUrl = $location.absUrl();


        // Object created to be sent when the directive is laoded
        var obj = {
            parent_name: $scope.parent_name
        };

        if ($scope.parent_name != '' && typeof $scope.type != 'undefined') {
            // sends the obj to confirm that a block was loaded
            $rootScope.$emit('rootScope:blocks', obj);
        }

    }

    baseController.$inject = ['$scope', 'APP_CONFIG', 'DATA', 'PATH_CONFIG', 'IMAGE_SIZE', '$stateParams', '$rootScope', '$translate', '$timeout', '$state', '$location', '$sce'];

    angular
        .module('bravoureAngularApp')
        .controller('baseController', baseController);

})();
