# Bravoure - Base Controller

## Use

Inside a directive:
        
    controller: function ($scope, $controller) {
        
        angular.extend(this, $controller('baseController', {
            $scope: $scope
        }));
        
    }

### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-base-controller": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
